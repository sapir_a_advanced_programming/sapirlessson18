#include <iostream>
#include <Windows.h>
#include <string>
#include <cstring>
#include <vector>
#include "Helper.h"

#define PATH_MAX 2048

std::string pwdFunction();
bool cdFunction(std::string path);
void createFunction(std::string fileName);
void lsFunction(std::string path);

void main()
{
	std::string userInput;
	std::vector<std::string> VuserInput;
	std::string currPath = pwdFunction();
	while (true)
	{
		std::cout << "<< ";
		std::getline(std::cin,userInput);
		VuserInput = Helper::get_words(userInput); //for mission 1
		if (VuserInput.at(0) == "pwd") // for mission 2
		{
			currPath = pwdFunction();
			std::cout << currPath << std::endl;
		}
		else if (VuserInput.at(0) == "cd")
		{
			if (cdFunction(currPath))
			{
				currPath = VuserInput.at(1);
			}
			else
			{
				std::cout << "Error. The folder doesn't exists" << std::endl;
			}
		}
		else if (VuserInput.at(0) == "create")
		{
			std::string fileName = VuserInput.at(1);
			createFunction(fileName);
		}
		else if (VuserInput.at(0) == "ls")
		{
			lsFunction(currPath);
		}
		else
		{
			GetLastError();
			std::cout << "GoodBye :) \n";
			break;
		}
	}
	system("pause");
}

std::string pwdFunction()
{
	char path[MAX_PATH];
	DWORD status = 0;
	status = GetCurrentDirectory(MAX_PATH, path);//Getting the path of the current dicantory
	if (!status)
	{
		throw std::invalid_argument("Can't get the current directory, please try again\n");
	}
	return std::string(path);//In bytes making it a string
}

bool cdFunction(std::string path)
{
	DWORD exists = GetFileAttributesA(path.c_str());
	if (exists == INVALID_FILE_ATTRIBUTES)
	{
		return false;  //something is wrong with your path!
	}
	if (exists & FILE_ATTRIBUTE_DIRECTORY)
	{
		return true;   // this is a directory!
	}
	return false;    // this is not a directory!
}

void createFunction(std::string fileName)
{
	HANDLE handleFile = CreateFile(fileName.c_str(), FILE_READ_DATA, FILE_SHARE_READ,NULL, CREATE_ALWAYS, 0, NULL);//Always creating file.
	if (handleFile != INVALID_HANDLE_VALUE)
	{
		std::cout << "Success\n";
	}
	else
	{
		CloseHandle(handleFile);
	}
	CloseHandle(handleFile);
}

void lsFunction(std::string path)
{
	std::string newPath = path + "\\*";
	WIN32_FIND_DATA data;
	HANDLE hFind = FindFirstFile(newPath.c_str(), &data);      // DIRECTORY

	if (hFind != INVALID_HANDLE_VALUE) 
	{
		do 
		{
			std::cout << data.cFileName << std::endl;
		} 
		while (FindNextFile(hFind, &data));
		FindClose(hFind);
	}
}
